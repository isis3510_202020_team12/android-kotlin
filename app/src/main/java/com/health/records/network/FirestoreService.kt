package com.health.records.network

import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ktx.firestoreSettings
import com.health.records.model.Appointment
import com.health.records.model.Diagnosis
import com.health.records.model.Treatment
import com.health.records.model.User
import kotlinx.coroutines.tasks.await
import java.lang.Exception

const val APPOINTMENT_COLLECTION_NAME = "appointments"
const val USER_COLLECTION_NAME = "users"
const val DIAGNOSIS_COLLECTION_NAME = "diagnosis"
const val TREATMENTS_COLLECTION_NAME = "treatments"


class FirestoreService(val firebaseFirestore: FirebaseFirestore) {

    init {
        val settings = firestoreSettings {
            isPersistenceEnabled = true
        }
        firebaseFirestore.firestoreSettings = settings
    }

    fun setDocument(data: Any, collectionName: String, callback: Callback<Void>) {
        firebaseFirestore.collection(collectionName).document().set(data)
            .addOnSuccessListener { callback.onSuccess(null) }
            .addOnFailureListener { exception -> callback.onFailed(exception) }
    }

    fun createUser(data: Any, collectionName: String, id: String, callback: Callback<Void>) {
        firebaseFirestore.collection(collectionName).document(id).set(data)
            .addOnSuccessListener { callback.onSuccess(null) }
            .addOnFailureListener { exception -> callback.onFailed(exception) }
    }

    suspend fun createAppointment (userId: String, data: Appointment): Boolean {
        return try {
            firebaseFirestore.collection(USER_COLLECTION_NAME)
                    .document(userId)
                    .collection(APPOINTMENT_COLLECTION_NAME)
                    .add(data)
                    .await()
            return true
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

    suspend fun getDiagnosisById (userId: String, diagnosisId: String): Diagnosis? {
        return try {
            val data = firebaseFirestore.collection(USER_COLLECTION_NAME)
                .document(userId)
                .collection(DIAGNOSIS_COLLECTION_NAME)
                .document(diagnosisId)
                .get()
                .await()
            data.toObject(Diagnosis::class.java)
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    suspend fun getDiagnosisTreatments (userId: String, diagnosisId: String): List<Treatment> {
        return try {
            val data = firebaseFirestore.collection(USER_COLLECTION_NAME)
                .document(userId)
                .collection(DIAGNOSIS_COLLECTION_NAME)
                .document(diagnosisId)
                .collection(TREATMENTS_COLLECTION_NAME)
                .get()
                .await()
            data.toObjects(Treatment::class.java)
        } catch (e: Exception) {
            e.printStackTrace()
            emptyList()
        }
    }

    suspend fun getAllDiagnosis(id: String): List<Diagnosis> {
        return try {
            val data = firebaseFirestore.collection(USER_COLLECTION_NAME)
                .document(id)
                .collection(DIAGNOSIS_COLLECTION_NAME)
                .get()
                .await()
            data.toObjects(Diagnosis::class.java)
        } catch (e: Exception) {
            e.printStackTrace()
            emptyList()
        }
    }

    suspend fun getAppointments(id: String): List<Appointment> {
        return try {
            val data = firebaseFirestore.collection(USER_COLLECTION_NAME)
                .document(id)
                .collection(APPOINTMENT_COLLECTION_NAME)
                .get()
                .await()
            data.toObjects(Appointment::class.java)
        } catch (e: Exception) {
            e.printStackTrace()
            emptyList()
        }
    }

    suspend fun findUserById(id: String): User? {
        return try {
            val data = firebaseFirestore.collection(USER_COLLECTION_NAME)
                .document(id)
                .get()
                .await()
            data.toObject(User::class.java)
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    suspend fun createDiagnosis(sourceId: String, diagnosis: Diagnosis): String {
        return try {
            val data = firebaseFirestore.collection(USER_COLLECTION_NAME)
                .document(sourceId)
                .collection(DIAGNOSIS_COLLECTION_NAME)
                .add(diagnosis)
                .await()
            data.id
        } catch (e: Exception) {
            e.printStackTrace()
            ""
        }
    }

    suspend fun createDiagnosisAndTreatments(sourceId: String,
                                             diagnosis: Diagnosis,
                                             treatments: List<Treatment>){
        return try {
            val data = firebaseFirestore.collection(USER_COLLECTION_NAME)
                .document(sourceId)
                .collection(DIAGNOSIS_COLLECTION_NAME)
                .add(diagnosis)
                .await()
            val batch = firebaseFirestore.batch()
            val collection = firebaseFirestore.collection(USER_COLLECTION_NAME)
                .document(sourceId)
                .collection(DIAGNOSIS_COLLECTION_NAME)
                .document(data.id)
                .collection(TREATMENTS_COLLECTION_NAME)
            treatments.forEach{ treatment ->
                val ref = collection.document()
                batch.set(ref, treatment)
            }
            val x = batch.commit().await()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    suspend fun createTreatments(sourceId: String, diagnosisId: String, treatments: List<Treatment>) {
        return try {
            val batch = firebaseFirestore.batch()
            val collection = firebaseFirestore.collection(USER_COLLECTION_NAME)
                .document(sourceId)
                .collection(DIAGNOSIS_COLLECTION_NAME)
                .document(diagnosisId)
                .collection(TREATMENTS_COLLECTION_NAME)
            treatments.forEach{ treatment ->
                val ref = collection.document()
                batch.set(ref, treatment)
            }
            val x = batch.commit().await()

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}