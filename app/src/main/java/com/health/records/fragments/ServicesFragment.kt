package com.health.records.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.health.records.ConnectivityCheck
import com.health.records.R
import com.health.records.databinding.FragmentServicesBinding


/**
 * A simple [Fragment] subclass.
 * Use the [ServicesFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ServicesFragment : Fragment() {

    private lateinit var binding: FragmentServicesBinding

    private lateinit var viewModel: ServicesViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_services,
            container,
            false
        )

        viewModel = ViewModelProvider(this).get(ServicesViewModel::class.java)

        val isConnected = ConnectivityCheck.isOnline(requireContext())
        if (!isConnected) {
            Toast.makeText(requireContext(), "There is no internet connection", Toast.LENGTH_LONG).show()
        }

        binding.nearbyHospitalsCard.setOnClickListener { startMapsActivity() }

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        binding.serviceActionProgress.visibility = View.GONE
        val isConnected = ConnectivityCheck.isOnline(requireContext())
        if (!isConnected) {
            Toast.makeText(requireContext(), "There is no internet connection", Toast.LENGTH_LONG).show()
        }
    }

    private fun startMapsActivity () {
        val isConnected = ConnectivityCheck.isOnline(requireContext())
        if (isConnected) {
            binding.serviceActionProgress.visibility = View.VISIBLE
            val action = ServicesFragmentDirections.actionIcServicesToNearbyHospitalsActivity()
            findNavController().navigate(action)
        } else {
            Toast.makeText(requireContext(), "To use this feature you need internet connection", Toast.LENGTH_LONG).show()
        }
    }
}