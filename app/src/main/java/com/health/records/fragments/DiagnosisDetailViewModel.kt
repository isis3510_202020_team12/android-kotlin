package com.health.records.fragments

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.health.records.model.Diagnosis
import com.health.records.model.Treatment
import com.health.records.network.FirestoreService
import kotlinx.coroutines.launch
import java.lang.Exception

class DiagnosisDetailViewModel (val diagnosisId: String): ViewModel() {

    private val _diagnosis = MutableLiveData<Diagnosis>()
    val diagnosis: LiveData<Diagnosis>
        get () = _diagnosis

    private val _treatments = MutableLiveData<List<Treatment>>()
    val treatments: LiveData<List<Treatment>>
        get () = _treatments

    private var firestoreService: FirestoreService

    private var auth: FirebaseAuth

    init {
        firestoreService = FirestoreService(FirebaseFirestore.getInstance())
        auth = FirebaseAuth.getInstance()
        loadDiagnosis()
        loadTreatments()
    }

    private fun loadDiagnosis () {
        viewModelScope.launch {
            try {
                val userId = auth.uid.toString()
                _diagnosis.value = firestoreService.getDiagnosisById(userId, diagnosisId)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun loadTreatments () {
        viewModelScope.launch {
            try {
                val userId = auth.uid.toString()
                _treatments.value = firestoreService.getDiagnosisTreatments(userId, diagnosisId)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}