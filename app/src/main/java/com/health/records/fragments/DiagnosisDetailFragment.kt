package com.health.records.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.health.records.R
import com.health.records.adapter.treatment.TreatmentAdapter
import com.health.records.databinding.FragmentDiagnosisDetailBinding
import com.health.records.model.Diagnosis


/**
 * A simple [Fragment] subclass.
 * Use the [DiagnosisDetailFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DiagnosisDetailFragment : Fragment() {

    private lateinit var viewModel: DiagnosisDetailViewModel

    private lateinit var binding: FragmentDiagnosisDetailBinding

    private lateinit var treatmentAdapter: TreatmentAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_diagnosis_detail,
            container,
            false
        )

        val diagnosisArgs by navArgs<DiagnosisDetailFragmentArgs>()

        val viewModelFactory = DiagnosisDetailViewModelFactory(diagnosisArgs.diagnosisId)
        viewModel = ViewModelProvider(this, viewModelFactory)
            .get(DiagnosisDetailViewModel::class.java)

        val linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

        viewModel.treatments.observe(this.viewLifecycleOwner) {
            treatmentAdapter = TreatmentAdapter(it)
            binding.recyclerViewTreatment.apply {
                adapter = treatmentAdapter
                layoutManager = linearLayoutManager
            }
        }

        viewModel.diagnosis.observe(this.viewLifecycleOwner, this::fillDiagnosisData)

        return binding.root
    }

    private fun fillDiagnosisData (diagnosis: Diagnosis) {
        val state = if (diagnosis.isCurrent) "Current" else "Closed"
        val stateColor = if (diagnosis.isCurrent) R.style.txtCurrent else R.style.txtClosed
        binding.txtDiagnosisNameDetail.text = diagnosis.name
        binding.txtStateDiagnosisDetail.text = state
        binding.txtStateDiagnosisDetail.setTextAppearance(stateColor)
        binding.txtDiagnosisDescription.text = diagnosis.description
    }
}