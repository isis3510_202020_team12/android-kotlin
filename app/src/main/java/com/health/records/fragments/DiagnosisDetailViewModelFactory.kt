package com.health.records.fragments

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import java.lang.IllegalArgumentException

class DiagnosisDetailViewModelFactory (
    private val diagnosisId: String
): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DiagnosisDetailViewModel::class.java)){
            return DiagnosisDetailViewModel(diagnosisId) as T
        } else {
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }
}