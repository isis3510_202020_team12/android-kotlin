package com.health.records.fragments

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.health.records.R
import com.health.records.adapter.treatment.AddTreatmentAdapter
import com.health.records.databinding.FragmentCreateDiagnosisBinding
import com.health.records.model.Treatment
import kotlinx.android.synthetic.main.treatment_dialog.view.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.health.records.model.Diagnosis
import java.util.*


/**
 * A simple [Fragment] subclass.
 * Use the [CreateDiagnosisFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CreateDiagnosisFragment : Fragment() {

    private lateinit var binding : FragmentCreateDiagnosisBinding
    private lateinit var viewModel : CreateDiagnosisViewModel


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_create_diagnosis,
            container,
            false
        )

        viewModel = ViewModelProvider(this).get(CreateDiagnosisViewModel::class.java)

        val diagnosisArgs by navArgs<CreateDiagnosisFragmentArgs>()


        // lets create a dialog for the creation of a treatment
        binding.addTreatmentCard.setOnClickListener {
            //val action = CreateDiagnosisDirections.actionCreateDiagnosisToCreateTreatmentFragment()
            //findNavController().navigate(action)

            //lets inflate the dialog with custom view
            val mDialogView = LayoutInflater.from(requireContext())
                .inflate(R.layout.treatment_dialog,null)

            val mBuilder = AlertDialog.Builder(requireContext())
                .setView(mDialogView)

            val mAlertDialog = mBuilder.show()


            mDialogView.btnCancel.setOnClickListener {
                mAlertDialog.dismiss()
            }

            mDialogView.prescriptionDate.setOnClickListener {
                    val calendar = Calendar.getInstance()
                    val day = calendar.get(Calendar.DAY_OF_MONTH)
                    val month = calendar.get(Calendar.MONTH)
                    val year = calendar.get(Calendar.YEAR)

                    val datePicker = DatePickerDialog(
                        requireContext(),
                        { view, y, m, d ->
                            val dateText = "$d/${m + 1}/$y"
                            mDialogView.prescriptionDate.setText(dateText)
                            val chosenDate = Calendar.getInstance()
                            chosenDate.set(y, m, d)
                        },
                        year, month, day
                    )
                    datePicker.show()
                }

            mDialogView.btnConfirm.setOnClickListener {
                val name =  mDialogView.txtTreatmentName_register.text.toString()
                val type =  mDialogView.txtTreatmentType_register.text.toString()
                val dosage =  mDialogView.txtTreatmentDosage_register.text.toString()
                val totalDosage =  mDialogView.txtTreatmentTotalDosage_register.text.toString()
                val duration =  mDialogView.txtTreatmentDuration_register.text.toString()
                val times =  mDialogView.txtTreatmentTimes_register.text.toString()
                val comments =  mDialogView.txtTreatmentComments_register.text.toString()
                val prescriptionDate = mDialogView.prescriptionDate.text.toString()
                if( listOf(name,type,dosage,totalDosage,duration,times,comments,prescriptionDate).all{it.isNotEmpty()}) {
                    val calendar = Calendar.getInstance()
                    val (day,month,year) = prescriptionDate.split("/").map { it.toInt() }
                    calendar.set(year,month,day)
                    val treatment = Treatment(comments,
                        dosage,
                        0,
                        duration,
                        name,
                        calendar.time,
                        Date(),
                        times,
                        totalDosage.toInt(),
                        type
                    )
                    viewModel.addtreatment(treatment)
                    Log.e("Error", viewModel.treatments.value!!.size.toString())
                    mAlertDialog.dismiss()
                }
                else
                    Toast.makeText(requireContext(),"Please don't leave empty fields", Toast.LENGTH_SHORT).show()

            }
        }

        val layoutmanager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

        viewModel.treatments.observe(this.viewLifecycleOwner, Observer{
            val diagnosisAdapter = AddTreatmentAdapter(it)
            val myList = binding.recyclerViewAddTreatment
            myList.adapter = diagnosisAdapter
            myList.layoutManager = layoutmanager
        })

        binding.buttonConfirm.setOnClickListener {
            var diagnosis = Diagnosis()
            diagnosis.description = binding.txtDiagnosisDescription.text.toString()
            diagnosis.isCurrent = true
            diagnosis.name = binding.txtDiagnosisNameRegister.text.toString()
            diagnosis.description = binding.txtDiagnosisDescription.text.toString()

            val list = listOf(diagnosis.description, diagnosis.name,diagnosis.description)

            if(list.all { it.isNotEmpty() }) {
                viewModel.setDiagnosis(diagnosis)
                viewModel.createDiagnosisDataBase(diagnosisArgs.sourceId)
                val action = CreateDiagnosisFragmentDirections.actionCreateDiagnosisToIcProfile()
                findNavController().navigate(action)
            }
            else{
                Toast.makeText(requireContext(),"Please don't leave empty fields", Toast.LENGTH_SHORT).show()
            }

        }


        return binding.root
    }
}