package com.health.records.fragments

import android.app.DatePickerDialog
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.health.records.R
import com.health.records.databinding.FragmentAppointmentCreationBinding
import java.util.*

/**
 * A simple [Fragment] subclass.
 * Use the [AppointmentCreationFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AppointmentCreationFragment : Fragment() {

    private lateinit var viewModel: AppointmentCreationViewModel

    private lateinit var binding: FragmentAppointmentCreationBinding

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        viewModel = ViewModelProvider(this).get(AppointmentCreationViewModel::class.java)

        binding = DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_appointment_creation,
                container,
                false
        )

        viewModel.hospitals.observe(this.viewLifecycleOwner) {
            setEditAutoComplete(binding.inputHospital, it)
        }

        viewModel.types.observe(this.viewLifecycleOwner) {
            setEditAutoComplete(binding.inputType, it)
        }

        binding.editTextDate.setOnClickListener {
            showDatePicker()
        }

        binding.confirmButton.setOnClickListener {
            val res = viewModel.onConfirm(
                    binding.inputHospital.text.toString(),
                    binding.inputType.text.toString()
            )
            Toast.makeText(requireContext(), res, Toast.LENGTH_SHORT).show()
            if (res == null) {
                val action = AppointmentCreationFragmentDirections.actionAppointmentCreationFragmentToIcAppointments()
                findNavController().navigate(action)
            }
        }

        return binding.root
    }

    private fun setEditAutoComplete (view: AutoCompleteTextView, items: List<String>) {
        val arrayAdapter = ArrayAdapter(
                requireContext(),
                android.R.layout.select_dialog_item,
                items
        )
        view.apply {
            threshold = 0
            setAdapter(arrayAdapter)
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun showDatePicker () {
        val calendar = Calendar.getInstance()
        val day = calendar.get(Calendar.DAY_OF_MONTH)
        val month = calendar.get(Calendar.MONTH)
        val year = calendar.get(Calendar.YEAR)

        val datePicker = DatePickerDialog(
                requireContext(),
                { view, y, m, d ->
                    val dateText = "$d/${m + 1}/$y"
                    binding.editTextDate.setText(dateText)
                    val chosenDate = Calendar.getInstance()
                    chosenDate.set(y, m, d)
                    viewModel.onChoseDate(chosenDate.time)
                },
                year, month, day
        )
        datePicker.show()
    }

}