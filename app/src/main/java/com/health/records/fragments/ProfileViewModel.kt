package com.health.records.fragments

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.health.records.model.Diagnosis
import com.health.records.model.User
import com.health.records.network.FirestoreService
import kotlinx.coroutines.launch
import java.lang.Exception

class ProfileViewModel: ViewModel() {

    private var _userId = MutableLiveData<String>()
    val userId: LiveData<String>
        get () = _userId

    private var _user = MutableLiveData<User>()
    val user: LiveData<User>
        get () = _user


    private var _diagnosis = MutableLiveData<List<Diagnosis>>()
    val diagnosis: LiveData<List<Diagnosis>>
        get () = _diagnosis


    private var firestoreService: FirestoreService

    private var auth: FirebaseAuth

    init {
        firestoreService = FirestoreService(FirebaseFirestore.getInstance())
        auth = FirebaseAuth.getInstance()
        _userId.value = auth.uid.toString()
        loadUserInfo()
        loadDiagnosisInfo()
    }

    fun loadUserInfo () {
        viewModelScope.launch {
            try {
                val id = auth.uid.toString()
                _user.value = firestoreService.findUserById(id)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun loadDiagnosisInfo(){
        viewModelScope.launch {
            try {
                val id = auth.uid.toString()
                _diagnosis.value= firestoreService.getAllDiagnosis(id)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun logout () {
        auth.signOut()
    }


}