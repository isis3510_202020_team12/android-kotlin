package com.health.records.fragments

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.firestore.FirebaseFirestore
import com.health.records.model.Diagnosis
import com.health.records.model.Treatment
import com.health.records.network.FirestoreService
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.lang.Exception

class CreateDiagnosisViewModel: ViewModel(){

    private val _diagnosis = MutableLiveData<Diagnosis>()
    val diagnosis : LiveData<Diagnosis>
        get() = _diagnosis

    private val _treatments = MutableLiveData<MutableList<Treatment>>()
    val treatments : LiveData<MutableList<Treatment>>
        get() = _treatments

    private var firestoreService: FirestoreService

    init{
        firestoreService = FirestoreService(FirebaseFirestore.getInstance())
        _treatments.value = mutableListOf()
    }

    fun addtreatment( treatment: Treatment){
        val new = _treatments.value!!
        new.add(treatment)
        _treatments.value = new
    }


    fun setDiagnosis(diagnosis: Diagnosis){
        _diagnosis.value = diagnosis
    }

    fun createDiagnosisDataBase(sourceId: String) {
        viewModelScope.launch {
            try {
                val diagnosisId= async {firestoreService.createDiagnosis(sourceId, _diagnosis.value!!)}
                firestoreService.createTreatments(sourceId, diagnosisId.await(),_treatments.value!!)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }



}