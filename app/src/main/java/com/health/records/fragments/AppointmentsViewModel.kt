package com.health.records.fragments

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.health.records.model.Appointment
import com.health.records.network.FirestoreService
import kotlinx.coroutines.launch
import java.lang.Exception

class AppointmentsViewModel: ViewModel() {

    private val _appointments = MutableLiveData<List<Appointment>>()
    val appointments: LiveData<List<Appointment>>
        get () = _appointments

    private var firestoreService: FirestoreService
    private var auth: FirebaseAuth

    init {
        firestoreService = FirestoreService(FirebaseFirestore.getInstance())
        auth = FirebaseAuth.getInstance()
        loadAppointments()
    }

    fun loadAppointments () {
        viewModelScope.launch {
            try {
                val id = auth.uid.toString()
                _appointments.value = firestoreService.getAppointments(id)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

}