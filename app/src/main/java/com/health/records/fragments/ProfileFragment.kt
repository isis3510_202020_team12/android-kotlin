package com.health.records.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.onNavDestinationSelected
import androidx.recyclerview.widget.LinearLayoutManager
import com.health.records.LoginActivity
import com.health.records.R
import com.health.records.adapter.diagnosis.DiagnosisAdapter
import com.health.records.databinding.FragmentProfileBinding
import com.squareup.picasso.Picasso


/**
 * A simple [Fragment] subclass.
 * Use the [ProfileFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ProfileFragment : Fragment() {

    private lateinit var binding: FragmentProfileBinding

    private lateinit var viewModel: ProfileViewModel

    lateinit var diagnosisAdapter: DiagnosisAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_profile,
            container,
            false
        )

        viewModel = ViewModelProvider(this).get(ProfileViewModel::class.java)

        viewModel.user.observe(this.viewLifecycleOwner, Observer {
            val name = "${it.name} ${it.lastName}"
            val height = "${it.height} cm"
            val weight = "${it.weight} kg"
            val age = "${it.age} years"
            val imageUrl = it.image
            binding.textNameId.text = name
            binding.heightTextValue.text = height
            binding.weightTextValue.text = weight
            binding.ageTextValue.text = age
            binding.bloodTextValue.text = it.blood
            Picasso.get().load(imageUrl).into(binding.profileImageId)
        })

        val layoutmanager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

        viewModel.diagnosis.observe(this.viewLifecycleOwner, Observer {
            Log.i("error", "" + it.size)
            diagnosisAdapter = DiagnosisAdapter(it, this)
            val myList = binding.recyclerViewDiagnosis
            myList.adapter = diagnosisAdapter
            myList.layoutManager = layoutmanager
        })

        binding.addDiagnosisFloatingButton.setOnClickListener {
            val sourceId = viewModel.userId.value!!
            val action = ProfileFragmentDirections.actionIcProfileToCreateDiagnosis(
                sourceId
            )
            findNavController().navigate(action)
        }

        binding.moreOptions.setOnClickListener(this::showMoreOptions)


        return binding.root
    }

    private fun showMoreOptions (view: View) {
        val popup = PopupMenu(requireContext(), view)
        popup.setOnMenuItemClickListener {
            val id = it.itemId
            when (id) {
                R.id.logout -> {
                    logout()
                    return@setOnMenuItemClickListener true
                }
                else -> {
                    return@setOnMenuItemClickListener false
                }
            }
        }
        popup.inflate(R.menu.profile_options_menu)
        popup.show()
    }

    private fun logout () {
        viewModel.logout()
        val goToLogin = Intent(activity, LoginActivity::class.java)
        startActivity(goToLogin)
    }

    fun goToDiagnosisDetail (diagnosisId: String) {
        val sourceId = viewModel.userId.value!!
        val action = ProfileFragmentDirections.actionIcProfileToDiagnosisDetailFragment(
            sourceId,
            diagnosisId
        )
        findNavController().navigate(action)
    }

    override fun onStart() {
        super.onStart()
        viewModel.loadUserInfo()
        viewModel.loadDiagnosisInfo()
    }

}