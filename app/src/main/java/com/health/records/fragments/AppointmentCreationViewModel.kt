package com.health.records.fragments

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.health.records.model.Appointment
import com.health.records.network.FirestoreService
import kotlinx.coroutines.launch
import java.util.*

class AppointmentCreationViewModel: ViewModel() {

    private val temporalHospitals = listOf("Santa Fe","Clínica del Country","Reina Sofía")

    private val temporalTypes = listOf("General", "Urgencias", "Prioritaria", "Especializada")

    private val _hospitals = MutableLiveData<List<String>>()
    val hospitals: LiveData<List<String>>
        get () = _hospitals

    private val _types = MutableLiveData<List<String>>()
    val types: LiveData<List<String>>
        get () = _types

    private val _selectedDate = MutableLiveData<Date>()
    val selectedDate: LiveData<Date>
        get () = _selectedDate


    private var firestoreService: FirestoreService

    private var auth: FirebaseAuth

    init {
        firestoreService = FirestoreService(FirebaseFirestore.getInstance())
        auth = FirebaseAuth.getInstance()
        _hospitals.value = temporalHospitals
        _types.value = temporalTypes

    }

    fun onChoseDate (chosenDate: Date) {
        _selectedDate.value = chosenDate
    }

    fun onConfirm (selectedHospital: String?, selectedType: String?): String? {
        val date = _selectedDate.value
        if ((selectedHospital != null && selectedHospital.isNotEmpty()) &&
                (selectedType != null && selectedType.isNotEmpty()) &&
                date != null) {

            if (!(selectedHospital in temporalHospitals)) {
                return "The entered hospital must be valid"
            }

            if (!(selectedType in temporalTypes)) {
                return "The entered type must be valid"
            }

            viewModelScope.launch {
                val id = auth.uid.toString()
                val appointment = Appointment()
                appointment.date = date
                appointment.hospital = selectedHospital
                appointment.type = selectedType
                firestoreService.createAppointment(id, appointment)
            }
            return null
        } else {
            return "All fields must be non-empty"
        }
    }

}