package com.health.records.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.health.records.R
import com.health.records.adapter.appointment.AppointmentAdapter
import com.health.records.databinding.FragmentAppointmentsBinding

/**
 * A simple [Fragment] subclass.
 * Use the [AppointmentsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AppointmentsFragment : Fragment() {

    private lateinit var binding: FragmentAppointmentsBinding

    private lateinit var appointmentAdapter : AppointmentAdapter

    private lateinit var viewModel: AppointmentsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_appointments,
            container,
            false
        )

        viewModel = ViewModelProvider(this).get(AppointmentsViewModel::class.java)

        val linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

        viewModel.appointments.observe(this.viewLifecycleOwner) {
            appointmentAdapter = AppointmentAdapter(it)
            binding.recyclerViewAppointments.apply {
                adapter = appointmentAdapter
                layoutManager = linearLayoutManager
            }
        }

        binding.addAppointmentButton.setOnClickListener {
            goToAppointmentCreation()
        }

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        viewModel.loadAppointments()
    }

    private fun goToAppointmentCreation () {
        val action = AppointmentsFragmentDirections.actionIcAppointmentsToAppointmentCreationFragment()
        findNavController().navigate(action)
    }
}