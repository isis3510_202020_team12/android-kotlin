package com.health.records

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.FirebaseFirestore
import com.health.records.model.User
import com.health.records.network.Callback
import com.health.records.network.FirestoreService
import com.health.records.network.USER_COLLECTION_NAME
import java.lang.Exception

class RegisterActivity : AppCompatActivity() {

    private lateinit var txtName: EditText
    private lateinit var txtLastName: EditText
    private lateinit var txtEmail: EditText
    private lateinit var txtPassword: EditText
    private lateinit var progressBar: ProgressBar
    private lateinit var dbReference: DatabaseReference
    private lateinit var database: FirebaseDatabase
    private lateinit var auth: FirebaseAuth
    private lateinit var firestoreService: FirestoreService




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        txtName = findViewById(R.id.txtName)
        txtLastName = findViewById(R.id.txtLastName)
        txtEmail = findViewById(R.id.txtEmailRegister)
        txtPassword = findViewById(R.id.txtPasswordRegister)

        progressBar = findViewById(R.id.register_progressBar)

        database = FirebaseDatabase.getInstance()
        auth = FirebaseAuth.getInstance()
        dbReference = database.reference.child("User")
        firestoreService = FirestoreService(FirebaseFirestore.getInstance())
    }


    fun register(view: View) {
        val name: String = txtName.text.toString()
        val lastName: String = txtLastName.text.toString()
        val email: String = txtEmail.text.toString()
        val password: String = txtPassword.text.toString()

        if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(lastName) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(
                password
            )
        ) {
            progressBar.visibility = View.VISIBLE


            auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    if (task.isComplete) {

                        val user: FirebaseUser? = auth.currentUser
                        verifyEmail(user)
                        val newUser = User()
                        newUser.lastName=lastName
                        newUser.name= name
                        saveUser(newUser, view, user?.uid.toString())
                    }
                }
        }
    }

    private fun saveUser(user: User, view: View, id: String) {
        firestoreService.createUser(user,USER_COLLECTION_NAME, id,object : Callback<Void> {
            override fun onSuccess(result: Void?) {
                action()
            }

            override fun onFailed(exception: Exception) {
            }
        })
    }

    private fun action() {
        startActivity(Intent(this, LoginActivity::class.java))
        progressBar.visibility = View.GONE
    }

    private fun verifyEmail(user: FirebaseUser?) {
        user?.sendEmailVerification()
            ?.addOnCompleteListener(this) { task ->
                if (task.isComplete) {
                    Toast.makeText(this, "Email sended", Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(this, "Error sendind the email", Toast.LENGTH_LONG).show()
                }
            }
    }
}