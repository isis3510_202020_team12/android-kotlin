package com.health.records

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import com.google.firebase.auth.EmailAuthCredential
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.health.records.localcache.PreferencesKeys
import com.health.records.localcache.SharedPreferencesUtils

private const val DEBUG_TAG = "NetworkStatusExample"


class LoginActivity : AppCompatActivity() {

    private lateinit var txtEmail: EditText
    private lateinit var txtPassword: EditText
    private lateinit var progressBar: ProgressBar
    private lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        txtEmail = findViewById(R.id.txtEmailSignIn)
        txtPassword = findViewById(R.id.txtPasswordSignIn)
        progressBar = findViewById(R.id.sign_in_progres_bar)
        auth = FirebaseAuth.getInstance()
    }

    fun forgotPassword(view: View) {
        startActivity(Intent(this, ForgotPasswordActivity::class.java))
    }

    fun register(view: View) {
        startActivity(Intent(this, RegisterActivity::class.java))
    }

    fun login(view: View) {
        loginUser()
    }


    private fun loginUser() {
        val user: String = txtEmail.text.toString()
        val password: String = txtPassword.text.toString()
        val isConnected = ConnectivityCheck.isOnline(this)
        if (!TextUtils.isEmpty(user) && !TextUtils.isEmpty(password) && isConnected) {
            progressBar.visibility = View.VISIBLE
            auth.signInWithEmailAndPassword(user, password)
                .addOnCompleteListener(this) { task ->
                    Log.v("Verbose", "Succesfull")
                    if (task.isSuccessful) {
                        action()
                    } else {
                        progressBar.visibility = View.GONE
                        Toast.makeText(this, "Error in the authentication", Toast.LENGTH_LONG)
                            .show()
                    }
                }
        } else {
            Toast.makeText(this, "There is no internet connection", Toast.LENGTH_LONG).show()
        }
    }

    private fun writeUserToken () {
        val user = auth.currentUser!!
        user.getIdToken(true)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val token = task.getResult().token!!
                    Log.i("Auth", "Saving: $token")
                    SharedPreferencesUtils.storeString(
                        this,
                        PreferencesKeys.AUTH_KEY,
                        token
                    )
                    action()
                } else {
                    progressBar.visibility = View.GONE
                    Toast.makeText(this, "Error in the authentication", Toast.LENGTH_LONG)
                        .show()
                }
            }
    }

    private fun action() {
        startActivity(Intent(this, MainActivity::class.java))
        progressBar.visibility = View.GONE
    }
}