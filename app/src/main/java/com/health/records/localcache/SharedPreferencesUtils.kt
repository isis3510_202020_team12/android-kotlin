package com.health.records.localcache

import android.content.Context
import androidx.preference.PreferenceManager

enum class PreferencesKeys (val stringVal: String) {
    AUTH_KEY("AUTH_KEY")
}

object SharedPreferencesUtils {

    fun storeString (context: Context, key: PreferencesKeys, value: String) {
        //val pref = context.getSharedPreferences("HealthRecords", Context.MODE_PRIVATE)
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = preferences.edit()
        editor.putString(key.stringVal, value)
        editor.apply()
    }

    fun getString (context: Context, key: PreferencesKeys): String? {
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        return preferences.getString(key.stringVal, "")
    }

}