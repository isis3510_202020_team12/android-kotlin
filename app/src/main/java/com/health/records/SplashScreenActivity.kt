package com.health.records

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.health.records.localcache.PreferencesKeys
import com.health.records.localcache.SharedPreferencesUtils
import java.lang.Exception

class SplashScreenActivity : AppCompatActivity() {

    private val auth = FirebaseAuth.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        val background = object : Thread(){
            override fun run() {
                try {
                    sleep(1500)
                    if (auth.currentUser != null) {
                        startActivity(Intent(baseContext, MainActivity::class.java))
                    } else {
                        val intent = Intent(baseContext, LoginActivity::class.java)
                        startActivity(intent)
                    }
                }
                catch (e: Exception){
                    e.printStackTrace()
                }
            }
        }
        background.start()
    }

    private fun verifyAuthToken () {
        val token = SharedPreferencesUtils.getString(
            this,
            PreferencesKeys.AUTH_KEY
        )

        Log.i("Auth", "Getting: ${auth.currentUser}")

        val intentLogin = Intent(baseContext, LoginActivity::class.java)

        if (token != null && token.isNotEmpty()) {
            auth.signInWithCustomToken(token)
                .addOnCompleteListener { task ->
                    Log.i("Auth", "Is Successfull: ${task.isSuccessful}")
                    if (task.isSuccessful) {
                        startActivity(Intent(this, MainActivity::class.java))
                    } else {
//                        Log.i("Auth", "Not successfull: ${task.result}")
                        startActivity(intentLogin)
                    }
                }
        } else {
            startActivity(intentLogin)
        }
    }
}