package com.health.records.model

import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.PropertyName
import java.util.*

data class Diagnosis(var registrationDate: Date = Date(),
                     @DocumentId var id: String ="",
                     @field:JvmField var isCurrent :Boolean=true,
                     var description:String = "",
                     var name:String=""
){
    constructor() : this(Date(), "",false, "", "")

}