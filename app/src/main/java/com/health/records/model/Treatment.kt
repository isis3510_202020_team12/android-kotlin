package com.health.records.model

import java.util.*

data class Treatment (
    var comments: String = "",
    var dosage: String = "",
    var dosesTaken: Int = 0,
    var duration: String = "",
    var name: String = "",
    var prescriptionDate: Date = Date(),
    var startDate: Date = Date(),
    var times: String = "",
    var totalDoses: Int = 0,
    var type: String = ""
)