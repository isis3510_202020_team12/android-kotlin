package com.health.records.model

import java.util.*


class Appointment {
    var date: Date = Date(System.currentTimeMillis())
    var type: String = ""
    var hospital: String = ""
}