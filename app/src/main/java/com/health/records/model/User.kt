package com.health.records.model

class User {
    var name:String = ""
    var lastName : String = ""
    var image: String = ""
    var blood: String = ""
    var age: Int = 0
    var height: Int = 0
    var weight: Int = 0
}