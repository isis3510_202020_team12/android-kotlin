package com.health.records.adapter.treatment

import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.health.records.R
import com.health.records.adapter.diagnosis.DiagnosisAdapter
import com.health.records.model.Treatment

class AddTreatmentAdapter (
    private val list: List<Treatment>
): RecyclerView.Adapter<AddTreatmentAdapter.AddTreatmentViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddTreatmentViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.new_treatment_row, parent, false)
        return AddTreatmentViewHolder(
            view
        )
    }

    override fun onBindViewHolder(holder: AddTreatmentViewHolder, position: Int) {
        val name = holder.view.findViewById<TextView>(R.id.txtTreatmentName)
        val type = holder.view.findViewById<TextView>(R.id.txtType)
        val dosage = holder.view.findViewById<TextView>(R.id.txtDosage)

        val treatment: Treatment = list[position]

        name.text = treatment.name
        type.text = treatment.type
        dosage.text = treatment.dosage
    }

    override fun getItemCount(): Int = list.size

    class AddTreatmentViewHolder(val view: View): RecyclerView.ViewHolder(view){

    }
}