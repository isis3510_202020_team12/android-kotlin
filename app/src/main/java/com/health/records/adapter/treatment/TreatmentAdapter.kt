package com.health.records.adapter.treatment

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.health.records.R
import com.health.records.databinding.TreatmentRowBinding
import com.health.records.model.Treatment
import java.util.*

class TreatmentAdapter (private val treatments: List<Treatment>):
    RecyclerView.Adapter<TreatmentAdapter.TreatmentViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TreatmentViewHolder {
//        val view = LayoutInflater
//            .from(parent.context)
//            .inflate(R.layout.treatment_row, parent, false)
        val inflater = LayoutInflater.from(parent.context)
        val binding = TreatmentRowBinding.inflate(inflater)

        return TreatmentViewHolder(binding)
    }

    override fun onBindViewHolder(holder: TreatmentViewHolder, position: Int) =
        holder.bindData(treatments[position])

    override fun getItemCount(): Int = treatments.size

    inner class TreatmentViewHolder (
        val binding: TreatmentRowBinding
    ): RecyclerView.ViewHolder(binding.root) {

        fun bindData (data: Treatment) {
            val taken = data.dosesTaken
            val total = data.totalDoses
            val percentage = 100 * (taken.toDouble()/total.toDouble()).toInt()
            val takenDosesText = "$taken/$total"

            val date = Calendar.getInstance()
            date.time = data.startDate
            val day = date.get(Calendar.DAY_OF_MONTH)
            val month = date.get(Calendar.MONTH)
            val year = date.get(Calendar.YEAR)
            val dateText = "$day/$month/$year"

            with (binding) {
                txtName.text = data.name
                typeValue.text = data.type
                dosageValue.text = data.dosage
                durationValue.text = data.duration
                startValue.text = dateText
                timesValue.text = data.times
                commentsValue.text = data.comments
                takenDosesValue.text = takenDosesText
                progressBar.progress = if (percentage == 0) 10 else percentage
            }
        }

    }
}