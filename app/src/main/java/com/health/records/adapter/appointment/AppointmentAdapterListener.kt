package com.health.records.adapter.appointment

import com.health.records.model.Appointment

interface AppointmentAdapterListener {
    fun onMarkAppointment(appointment: Appointment)
}