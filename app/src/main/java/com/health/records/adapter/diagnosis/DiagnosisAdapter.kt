package com.health.records.adapter.diagnosis

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.health.records.R
import com.health.records.fragments.ProfileFragment
import com.health.records.fragments.ProfileFragmentDirections
import com.health.records.fragments.ProfileViewModel
import com.health.records.model.Diagnosis

class DiagnosisAdapter(
    private val list: List<Diagnosis>,
    private val fragment: ProfileFragment
) : RecyclerView.Adapter<DiagnosisAdapter.DiagnosisViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DiagnosisViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.diagnosis_row, parent, false)
        return DiagnosisViewHolder(
            view
        )
    }

    override fun onBindViewHolder(holder: DiagnosisViewHolder, position: Int) {
        val name = holder.view.findViewById<TextView>(R.id.txtDiagnosis)
        val state = holder.view.findViewById<TextView>(R.id.txtDiagnosisState)

        val diagnosis: Diagnosis = list[position]

        name?.text = diagnosis.name
        state?.text = if (diagnosis.isCurrent) "Current" else "Closed"
        if(diagnosis.isCurrent)
            state?.setTextAppearance(R.style.txtCurrent)
        else
            state?.setTextAppearance(R.style.txtClosed)

        val card = holder.view.findViewById<CardView>(R.id.diagnosisCard)
        card.setOnClickListener {
            fragment.goToDiagnosisDetail(diagnosis.id)
        }
    }

    override fun getItemCount(): Int = list!!.size

    class DiagnosisViewHolder(val view: View) : RecyclerView.ViewHolder(view) {


    }
}

