package com.health.records.adapter.appointment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.health.records.R
import com.health.records.model.Appointment
import java.time.LocalDate
import java.time.ZoneId
import java.util.*


class AppointmentAdapter(private val list: List<Appointment>?) :
    RecyclerView.Adapter<AppointmentAdapter.AppointmentViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppointmentViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.appointment_row, parent, false)
        return AppointmentViewHolder(
            view
        )
    }

    override fun onBindViewHolder(holder: AppointmentViewHolder, position: Int) {
        val date = holder.view.findViewById<TextView>(R.id.txtDate)
        val location = holder.view.findViewById<TextView>(R.id.txtLocation)
        val specialty = holder.view.findViewById<TextView>(R.id.txtSpecialty)

        val appointment: Appointment = list!![position]

        val dateVal = Calendar.getInstance()
        dateVal.time = appointment.date
        val dayNum = dateVal.get(Calendar.DAY_OF_MONTH)
        val day = dateVal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault())
        val month = dateVal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault())
        val year = dateVal.get(Calendar.YEAR)
        val dateText = "$day, $month $dayNum, $year"

        date?.text = dateText
        location?.text = appointment.hospital
        specialty?.text = appointment.type
    }

    override fun getItemCount(): Int = list!!.size

    class AppointmentViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    }
}